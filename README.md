LexikCitiesBundle
=================

Fixtures
--------

No fixtures, just import the file `vendor/lexik/cities-bundle/Lexik/Bundle/CitiesBundle/Resources/data/lexik-cities.sql` in your database.

Configuration
-------------

lexik_cities:
    lexik_crud_mode:   false                           # true to enable lexik crud integration
    fos_elastica_mode: false                           # disabled by default - remove to enable it
        index_service: fos_elastica.index.website.city # elasticsearch index service

What's next
-----------

If you want to use the elasticsearch mode, take a look at the `Resources/doc/elasticsearch.sample.yml` for an example of elasticsearch index configuration.
Enable fos_elastica_mode in the bundle config.
And don't forget to populate elasticsearch index through the `php app/console fos:elastica:populate`.
