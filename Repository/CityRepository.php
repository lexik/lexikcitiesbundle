<?php

namespace Lexik\Bundle\CitiesBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * CityRepository
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 */
class CityRepository extends EntityRepository
{
    /**
     * @param string $zipcode
     *
     * @return array
     */
    public function getByZipcode($zipcode)
    {
        return $this
            ->createQueryBuilder('c')
            ->where('c.zipcode = :zipcode')
            ->setParameter('zipcode', $zipcode)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $query
     * @param int    $limit
     *
     * @return array
     */
    public function search($query, $limit = 10)
    {
        $qb = $this->createQueryBuilder('c');

        return $qb
            ->select('c, d')
            ->leftJoin('c.departement', 'd')
            ->where($qb->expr()->like('c.zipcode', ':query'))
            ->orWhere($qb->expr()->like('c.name', ':query'))
            ->setParameter('query', $query . '%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
