<?php

namespace Lexik\Bundle\CitiesBundle\Filter;

use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * CityType
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 */
class CityType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'filter_text',
                array(
                    'condition_pattern' => FilterOperands::STRING_BOTH,
                    'label' => 'Nom',
                )
            )
            ->add(
                'zipcode',
                'filter_text',
                array(
                    'condition_pattern' => FilterOperands::STRING_BOTH,
                    'label' => 'Code postal',
                )
            )
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'city';
    }
}
