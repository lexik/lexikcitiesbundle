<?php

namespace Lexik\Bundle\CitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Lexik\Bundle\CitiesBundle\Utils\Inflector;

/**
 * City
 *
 * @ORM\Entity(repositoryClass="Lexik\Bundle\CitiesBundle\Repository\CityRepository")
 * @ORM\Table(
 *     name="lexik_city_city",
 *     uniqueConstraints={ @ORM\UniqueConstraint(name="city_slug_idx", columns={"slug"}) }
 * )
 * @ORM\HasLifecycleCallbacks
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="zipcode")
     *
     * @Assert\Regex(pattern="/^\d{5}$/", message="Le code postal doit contenir 5 chiffres")
     */
    private $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="code")
     *
     * @Assert\Regex(pattern="/^[0-9AB]{5}$/", message="Le code commune doit contenir 5 caractères (0-9 A B)")
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="article", nullable=true)
     */
    private $article;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="indexed_name", nullable=true)
     */
    private $indexedName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="indexed_name_alt", nullable=true)
     */
    private $indexedNameAlt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="slug", nullable=false)
     */
    private $slug;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=12, scale=9, nullable=true)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=12, scale=9, nullable=true)
     */
    private $longitude;

    /**
     * @var Departement
     *
     * @ORM\ManyToOne(targetEntity="Departement", inversedBy="cities")
     */
    private $departement;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", name="ville_associee", nullable=true)
     */
    private $villeAssociee;

    /**
     * Get id
     *
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * Get zipcode
     *
     * @param string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set article
     *
     * @param string $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }

    /**
     * Get article
     *
     * @return string
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set indexedName
     *
     * @param string $name
     */
    public function setIndexedName($name)
    {
        $this->indexedName = $name;
    }

    /**
     * Get indexedName
     *
     * @return string
     */
    public function getIndexedName()
    {
        return $this->indexedName;
    }

    /**
     * Set indexedNameAlt
     *
     * @param string $name
     */
    public function setIndexedNameAlt($name)
    {
        $this->indexedNameAlt = $name;
    }

    /**
     * Get indexedNameAlt
     *
     * @return string
     */
    public function getIndexedNameAlt()
    {
        return $this->indexedNameAlt;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * Get latitude
     *
     * @param float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set departement
     *
     * @param Departement $departement
     */
    public function setDepartement(Departement $departement)
    {
        $this->departement = $departement;
    }

    /**
     * Get departement
     *
     * @return Departement
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Set villeAssociee
     *
     * @param boolean $villeAssociee
     *
     * @return City
     */
    public function setVilleAssociee($villeAssociee)
    {
        $this->villeAssociee = $villeAssociee;

        return $this;
    }

    /**
     * Get villeAssociee
     *
     * @return boolean
     */
    public function getVilleAssociee()
    {
        return $this->villeAssociee;
    }

    /**
     * Returns the city's full name with the right separator between name and article.
     *
     * @return string
     */
    public function getFullname()
    {
        if ($this->article) {
            $separator = $this->article == "L'" ? '' : ' ';
            return $this->article.$separator.$this->name;
        }

        return $this->name;
    }

    /**
     * Returns the city's full name.
     *
     * @return string
     */
    public function getFullnamebis()
    {
        if ($this->article) {
            return $this->article.' '.$this->name;
        }

        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s - %s', $this->zipcode, $this->name);
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $str = sprintf('%s-%s-%s', $this->zipcode, Inflector::unaccent($this->indexedNameAlt), $this->code);

        $this->slug = Inflector::slugify($str);
    }
}

