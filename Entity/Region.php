<?php

namespace Lexik\Bundle\CitiesBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Region
 *
 * @ORM\Entity()
 * @ORM\Table(name="lexik_city_region")
 */
class Region
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="string", name="number")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="indexed_name")
     */
    private $indexedName;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Departement", mappedBy="region")
     */
    private $departements;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->departements = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set indexedName
     *
     * @param string $name
     */
    public function setIndexedName($name)
    {
        $this->indexedName = $name;
    }

    /**
     * Get indexedName
     *
     * @return string
     */
    public function getIndexedName()
    {
        return $this->indexedName;
    }

    /**
     * Add departement
     *
     * @param Departement $departement
     */
    public function addDepartement(Departement $departement)
    {
        $this->departements[] = $departement;
    }

    /**
     * Get departements
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getDepartements()
    {
        return $this->departements;
    }
}
