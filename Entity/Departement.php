<?php

namespace Lexik\Bundle\CitiesBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Departement
 *
 * @ORM\Entity()
 * @ORM\Table(name="lexik_city_departement")
 */
class Departement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="string", name="number")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="indexed_name")
     */
    private $indexedName;

    /**
     * @var Region
     *
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="departements")
     */
    private $region;

    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="departement")
     */
    private $cities;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->cities = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set indexedName
     *
     * @param string $name
     */
    public function setIndexedName($name)
    {
        $this->indexedName = $name;
    }

    /**
     * Get indexedName
     *
     * @return string
     */
    public function getIndexedName()
    {
        return $this->indexedName;
    }

    /**
     * Set region
     *
     * @param \Lexik\Bundle\CitiesBundle\Entity\Region $region
     */
    public function setRegion(Region $region)
    {
        $this->region = $region;
    }

    /**
     * Get region
     *
     * @return \Lexik\Bundle\CitiesBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Add city
     *
     * @param City $city
     */
    public function addCity(City $city)
    {
        $this->cities[] = $city;
    }

    /**
     * Get cities
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->number.' - '.$this->name;
    }
}
