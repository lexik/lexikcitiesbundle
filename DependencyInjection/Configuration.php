<?php

namespace Lexik\Bundle\CitiesBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('lexik_cities');

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->booleanNode('lexik_crud_mode')
                    ->defaultFalse()
                ->end()
                ->arrayNode('fos_elastica_mode')
                    ->canBeEnabled()
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('index_service')
                            ->defaultValue('lexik_cities.index.city')
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
