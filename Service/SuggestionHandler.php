<?php

namespace Lexik\Bundle\CitiesBundle\Service;

use Elastica\SearchableInterface;
use FOS\ElasticaBundle\Elastica\Index;
use Lexik\Bundle\CitiesBundle\Entity\City;
use Lexik\Bundle\CitiesBundle\Repository\CityRepository;

/**
 * Handle suggestion queries.
 *
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class SuggestionHandler
{
    /**
     * @var CityRepository
     */
    protected $repository;

    /**
     * @var SearchableInterface
     */
    protected $index;

    /**
     * @param CityRepository      $repository
     * @param SearchableInterface $index
     */
    public function __construct(CityRepository $repository, SearchableInterface $index = null)
    {
        $this->repository = $repository;
        $this->index      = $index;
    }

    /**
     * Returns cities that match $query.
     *
     * @param string $query
     * @return array
     */
    public function getZipcodeSuggestions($query)
    {
        $data = array();

        if ($this->index) {

            $searchQuery = new \Elastica\Query\QueryString();
            $searchQuery->setParam('query', $query);
            $searchQuery->setParam('fields', array('zipcode'));

            $results = $this->index->search($searchQuery, 10)->getResults();

            foreach ($results as $result) {
                $source = $result->getSource();
                $data[] = array(
                    'suggest' => $source['zipcode'].' '.$source['fullname'],
                    'zipcode' => $source['zipcode'],
                    'city'    => $source['fullname'],
                );
            }

        } else {

            /** @var City $city */
            foreach ($this->repository->getByZipcode($query) as $city) {
                $data[] = array(
                    'suggest' => $city->getZipcode().' '.$city->getFullname(),
                    'zipcode' => $city->getZipcode(),
                    'city'    => $city->getFullname(),
                    'dept'    => $city->getDepartement()->getNumber(),
                    'id'      => $city->getId(),
                );
            }

        }

        return $data;
    }

    /**
     * Returns cities that match $query.
     *
     * @param string $query
     *
     * @return array
     */
    public function getCitySuggestions($query)
    {
        $data = array();

        if ($this->index) {

            $searchQuery = new \Elastica\Query\QueryString();
            $searchQuery->setParam('query', $query);
            $searchQuery->setDefaultOperator('AND');
            $searchQuery->setParam('fields', array('fullname', 'fullnamebis'));

            $results = $this->index->search($searchQuery, 10)->getResults();

            foreach ($results as $result) {
                $source = $result->getSource();
                $data[] = array(
                    'suggest' => $source['zipcode'].' '.$source['fullname'],
                    'zipcode' => $source['zipcode'],
                    'city'    => $source['fullname'],
                );
            }

        } else {

            /** @var City $city */
            foreach ($this->repository->search($query) as $city) {
                $data[] = array(
                    'suggest' => $city->getZipcode().' '.$city->getFullname(),
                    'zipcode' => $city->getZipcode(),
                    'city'    => $city->getFullname(),
                    'dept'    => $city->getDepartement()->getNumber(),
                    'id'      => $city->getId(),
                );
            }

        }

        return $data;
    }
}
