<?php

namespace Lexik\Bundle\CitiesBundle\Controller\Backend;

use Lexik\Bundle\CrudBundle\Controller\CrudController;
use Lexik\Bundle\CrudBundle\Routing\Route;
use Lexik\Bundle\CitiesBundle\Entity\City;
use Lexik\Bundle\CitiesBundle\Entity\Departement;
use Symfony\Component\Form\Form;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * CityController
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 *
 * @Route("/city", name="crud_city", serviceId="lexik_cities.crud.city")
 */
class CityController extends CrudController
{
    /**
     * {@inheritDoc}
     */
    protected function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'form_class_name'   => 'Lexik\Bundle\CitiesBundle\Form\CityType',
            'filter_class_name' => 'Lexik\Bundle\CitiesBundle\Filter\CityType',
        ));
    }

    /**
     * {@inheritDoc}
     */
    protected function persist($object, Form $form = null)
    {
        $object->setVilleAssociee(true);

        parent::persist($object, $form);
    }

    /**
     * Parameter needed in all templates.
     *
     * @param object $object
     *
     * @return array
     */
    protected function getTemplateExtraParameters($object = null)
    {
        return array(
            'activeMenu' => 'city',
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function getListExtraParameters()
    {
        return array_merge($this->getTemplateExtraParameters(), array());
    }

    /**
     * {@inheritDoc}
     */
    protected function getNewExtraParameters($object)
    {
        return array_merge($this->getTemplateExtraParameters(), array());
    }

    /**
     * {@inheritDoc}
     */
    protected function getEditExtraParameters($object)
    {
        return array_merge($this->getTemplateExtraParameters(), array());
    }
}
