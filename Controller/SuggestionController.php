<?php

namespace Lexik\Bundle\CitiesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * SuggestionController
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 */
class SuggestionController extends Controller
{
    /**
     * @param Request $request
     *
     * @throws HttpException
     * @return JsonResponse
     */
    public function zipcodeSuggestAction(Request $request)
    {
        if (!$query = $request->query->get('q', null)) {
            throw new HttpException(400);
        }

        return new JsonResponse(
            $this->get('lexik_cities.suggestion_handler')->getZipcodeSuggestions($query)
        );
    }

    /**
     * @param Request $request
     *
     * @throws HttpException
     * @return JsonResponse
     */
    public function citySuggestAction(Request $request)
    {
        if (!($query = $request->get('q', null)) || strlen($query) < 3) {
            throw new HttpException(400);
        }

        return new JsonResponse(
            $this->get('lexik_cities.suggestion_handler')->getCitySuggestions($query)
        );
    }
}
