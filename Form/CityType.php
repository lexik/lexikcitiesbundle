<?php

namespace Lexik\Bundle\CitiesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * CityType class.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 */
class CityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'required'  => true,
                'label' => 'Nom de la ville',
            ))
            ->add('article', 'choice', array(
                'required'  => false,
                'choices'   => array(
                    'L\''   => 'L\'',
                    'Le'    => 'Le',
                    'La'    => 'La',
                    'Les'   => 'Les',
                ),
                'label' => ' Article',
            ))
            ->add('departement', null, array(
                'required'  => true,
                'label' => 'Département',
            ))
            ->add('zipcode', null, array(
                'required'  => true,
                'label' => 'Code postal',
            ))
            ->add('latitude', null, array(
                'label' => ' Latitude',
                'precision' => 9,
            ))
            ->add('longitude', null, array(
                'label' => ' Longitude',
                'precision' => 9,
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'Lexik\Bundle\CitiesBundle\Entity\City',
            'cascade_validation' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'city';
    }
}
